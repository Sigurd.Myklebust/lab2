package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {
    // Deklarerer feltvariablane til klassen Pokemon (tilstanden til objektet)
    private String name; // Bruk private for feltvariablar, så kan du bruke get-metodar for å hente ut verdien
    private int strength;
    private int healthPoints;
    private int maxHealthPoints;
    private Random random; 

    public Pokemon(String name){ // Konstruktør
        this.name = name; // this.name refererer til feltvariabelen name, god praksis/kodestil å bruke
        // Vi endrar feltvariabelen (f. eks. this.name) til objektet vi opprettar, med verdiane vi sender inn gjennom konstruktøren
        // Så når vi sender inn new Pokemon("Pikachu"); vil feltvariabelen this.name endrast til verdien name som i dette tilfelle er Pikachu 
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));

    }

    // Bruk static for metodar der all relevant input kjem i form av argument, der det ikkje er behov for å ha tilgang til instansvariablar

    // Intialiserer alle metodene fra IPokemon (Interface Pokemon)
    public String getName() { 
        return this.name;
    }

    @Override
    public int getStrength() {
        return this.strength;
    }

    @Override
    public int getCurrentHP() {
        return this.healthPoints;
    }

    @Override
    public int getMaxHP() {
       return this.maxHealthPoints; // Intialiserer maxHealthPoints i konstruktøren
    }

    public boolean isAlive() {
        if (this.healthPoints > 0) { // Sjekke om Pokémonen er i live
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void attack(IPokemon target) {
        // Må bruke target.getName() for å hente ut navnet til Pokémonen som blir angrepet (visst ikkje får eg ut HP og Str osv. frå toString-metoden)
        System.out.println(this.name + " attacks " + target.getName() + ".");
        
        // damageInflicted = strength + 50% av strength * random
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian()); 
        target.damage(damageInflicted); // Bruker damage-metoden (lenger nede) med argumentet damageInflicted til å skade Pokémonen
 
        if (target.getCurrentHP() <= 0) { // Sjekker om Pokémonen er i live (brukar getCurrentHP-metoden)
            System.out.println(target.getName() + " is defeated by " + this.name);
            System.out.println("The winner is " + this.name + "!");
        }
    }

    @Override // Override er ein kommentar (gjer ingenting med koda) som seier at denne metoda skal bli implementert i Interface 
    // Fortel editoren (java) at han må leite etter kommentaren som Override referer til i Interface
    public void damage(int damageTaken) { 
        if (damageTaken > 0 && healthPoints > 0) { // damageTaken (argumentet sendt inn i metoda) må vere positivt, og healthPoints må vere større enn 0
            this.healthPoints -= damageTaken; // Så lenge dette er true, så skal healthPoints bli redusert med damageTaken
              
            if (this.healthPoints <= 0) { // Kan ikkje sitte igjen med negativ HP, så blir sett til 0
                this.healthPoints = 0;
                System.out.println(this.name + " takes " + damageTaken + " damage and is left with " + this.healthPoints + "/" + this.maxHealthPoints + " HP");

            }
            // Dersom healthPoints er større enn 0, skriv ein ut kor mykje skade Pokémonen tok, og hvor mykje HP Pokémonen har igjen
            else  {
                System.out.println(this.name + " takes " + damageTaken + " damage and is left with " + this.healthPoints + "/" + this.maxHealthPoints + " HP");

            }
        }  
    }

    @Override
    public String toString() {
        // Implementerer toString() for å få utskrift av objektet
        // Bruk String.format() for å formatere strengen
        String stringPokemon = this.name + " HP: (" + this.healthPoints + "/" + this.maxHealthPoints + ") STR: " + this.strength;
        return stringPokemon;
        
    }

}
