package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        pokemon1 = new Pokemon("Pikachu");
        pokemon2 = new Pokemon("Oddish");

        // Printar ut informasjon om begge pokemons (stats)
        System.out.println(pokemon1.toString());
        System.out.println(pokemon2.toString());
        System.out.println();

        // While-loop som køyrer så lenge begge pokemons er i live
        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            pokemon1.attack(pokemon2);
            
            if (!pokemon2.isAlive()) { // Avsluttar loop om ein av pokemons er død, slik at ikkje ein av pokemons blir angrepet etter at den er død
                break;
            }
            
            pokemon2.attack(pokemon1);
        }

        // Get pictures of the pokemons and show them
        // System.out.println(pokemon1.getPicture());
        // System.out.println(pokemon2.getPicture());

        // public static void getPicture() {

        //     // Get picture of the pokemon
        //     // https://stackoverflow.com/questions/1066589/iterate-through-enumeration
        //     for (Pokemon pokemon : Pokemon.values()) {
        //         System.out.println(pokemon.getPicture());
        //     }
        // }

    }
}
